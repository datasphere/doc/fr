# Configuration file for the Sphinx documentation builder.
from os import getcwd

# -- Project information -----------------------------------------------------
language = 'fr'

project = 'Datasphere'
slug = 'datasphere'
website = f'https://{slug}.readthedocs.io/'
mirror = f'https://{slug}.gitpages.huma-num.fr/doc/{language}/'
versions = f'https://readthedocs.org/projects/{slug}/downloads/'
repo = f'https://gitlab.huma-num.fr/{slug}/doc/{language}'
issues = f'{repo}/issues/'
changelog = f'{repo}/commits/'

copyright = 'CC BY-NC-SA 3.0'
author = 'Régis Witz'

release = '1.1'
version = '1.1.0'

rst_prolog = """
.. |project| replace:: {project}
.. |website| replace:: {website}
.. |mirror| replace:: {mirror}
.. |repo| replace:: {repo}
.. |version| replace:: {version}
.. |versions| replace:: {versions}
.. |changelog| replace:: {changelog}
.. |issues| replace:: {issues}
.. |copyright| replace:: {copyright}
""".format(
  project=project,
  website=website,
  mirror=mirror,
  repo=repo,
  version=version,
  versions=versions,
  changelog=changelog,
  issues=issues,
  copyright=copyright,
)

# -- General configuration ---------------------------------------------------

extensions = [
  'sphinx.ext.autodoc',       # HTML generation from docstrings
  'sphinx.ext.intersphinx',   # Link to other projects’ documentation
  'sphinxcontrib.plantuml',   # PlantUML syntax support
]

intersphinx_mapping = {
  'hera': ('https://datasphere.readthedocs.io/projects/hera/fr/latest', None),
  'hecate': ('https://datasphere.readthedocs.io/projects/hecate/fr/latest', None),
  'heimdall': ('https://datasphere.readthedocs.io/projects/heimdall/fr/latest', None),
  'datasphere-en': ('https://datasphere.readthedocs.io/en/latest', None),

  'python': ('https://docs.python.org/3/', None),
  'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'

# see: https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html
html_static_path = ['../resource']
html_css_files = ['css/candy.css']
#html_logo = 'https://sharedocs.huma-num.fr/wl/?id=CCbIwNL3LpgBrKXGtlFN6n2fbnESDkO4&fmode=download'
html_logo = 'https://randomwordgenerator.com/img/picture-generator/54e9d14b4e52a814f1dc8460962e33791c3ad6e04e50744172297ed39649c4_640.jpg'
html_theme_options = {
  'logo_only': True,
  'style_external_links': True,
}

# -- Options for EPUB output -------------------------------------------------
epub_show_urls = 'footnote'

# -- Options for PlantUML diagrams -------------------------------------------
## @see https://plantuml.com/fr/download
## @see https://pypi.org/project/sphinxcontrib-plantuml/#configuration
## @see .gitlab-ci.yml
## @see .readthedocs.yaml
## some distro's plantuml packages dont' support non-uml tags like @startjson
## or @startmindmap, so we download a later plantuml jar in the root folder,
## and then tell sphinxcontrib-plantuml to use it with the following line :
plantuml = 'java -jar %s/../../plantuml-gplv2-1.2024.3.jar' % getcwd()
## we want accessibility and customization, thus we want .svgs, not .pngs
plantuml_output_format = 'svg'
