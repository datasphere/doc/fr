.. _dco:

Certification de l'origine des contributions
============================================

L'origine d'une contribution peut être certifiée directement par le contributeur
ou la contributrice à un projet. Pour ce faire, il ou elle déclare qu'il ou
elle accepte pour *cette contribution uniquement* le texte publié à l'adresse
suivante : https://developercertificate.org/.
En pratique, chaque contributeur ou contributrice peut, et **devrait**,
terminer chacun de ses messages de commit par la ligne
``Signed-off-by: Nom complet <email>``.

Plus d'informations sont disponibles ici: https://wiki.linuxfoundation.org/dco.

Pour clarifier, le texte de la DCO équivaut en votre déclaration que :

- vous êtes le créateur ou la créatrice de votre contribution **ou** que vous
  reprenez un travail antérieur compatible avec la licence du projet ;
- vous comprenez que tout ce que vous partagez dans cette contribution est
  public, et sera redistribué sans restriction -ce qui inclut votre commentaire
  de commit, et donc le nom, le prénom et l'email avec lequel vous le signez.

Cette manière de faire est la méthode la plus simple et légère de faire les
choses au sein d'un cadre légal propre. Elle peut être très facilement mise
en place, aussi bien par l'équipe derrière un projet, que par ses contributeurs
et ses contributrices externes.
Elle est, de plus, recommandée par les principes d’ouverture des codes sources\ [#dinum]_.

.. [#dinum] Politique de contribution aux logiciels libres de l’État :
 https://www.numerique.gouv.fr/publications/politique-logiciel-libre/ouverture/
