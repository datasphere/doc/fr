.. _projects:

projets
=======



.. _projects_hera:

Hera
----

.. image:: https://sharedocs.huma-num.fr/wl/?id=kj3fW3WKOPJFMB5Z8DB3y0IYpKKzy0DN&fmode=download
   :target: ../../../projects/hera/fr/latest/
   :height: 7em

Hera propose un modèle permettant de traduire les données de n’importe quelle base de données externe, quel que soit son format ou sa thématique, sans toutefois imposer une conversion complète des schémas.

Hera est une modélisation minimale et générique des données, volontairement simple et resserée.
Elle ne cherche cependant pas à se substituer aux bases avec lesquels elle interagit.
Son objectif est de permettre la réutilisation des données à moindre coût.

:ref:`En savoir plus <hera:home>`


.. _projects_hecate:

Hecate
------

.. image:: https://sharedocs.huma-num.fr/wl/?id=zu51tqXtpv5e9lv8DXzu0Vrg9DLNKowD&fmode=download
   :target: ../../../projects/hecate/fr/latest/
   :height: 7em

Hecate n'est pas encore née …



.. _projects_heimdall:

Heimdall
--------

.. image:: https://sharedocs.huma-num.fr/wl/?id=bTXaJHOJwSa2swWiqdHxHPbfqMouu38u&fmode=download
   :target: ../../../projects/heimdall/fr/latest/
   :height: 7em

Heimdall est un outil permettant de convertir facilement une ou plusieurs bases de données d'un format à un autre.

Heimdall met à profit la modélisation offerte par :ref:`Hera <projects_hera>` pour rendre plus aisée et pérenne l'interopérabilité entre de multiple systèmes:

- les bases de données SQL (`MariaDB <https://mariadb.org/>`_, `MySQL <https://www.mysql.com/fr/>`_, `PostgreSQL <https://www.postgresql.org/>`_, …) ou NoSQL (*eg* `MongoDB <https://www.mongodb.com/fr-fr>`_, …),
- les API REST (*eg.* `Nakala <https://nakala.fr/>`_, `Gallica <https://api.bnf.fr/fr/api-document-de-gallica>`_, `GeoNames <https://www.geonames.org/export/web-services.html>`_, `OpenTheso <https://pactols.frantiq.fr/opentheso/openapi/doc/>`_, …),
- les systèmes permettant l'import/export de leurs données aux format CSV (*eg.* `tableurs <https://www.libreoffice.org/discover/calc/>`_, `Omeka <https://omeka.org/s/>`_, …), XML (*eg.* `corpus TEI <https://tei-c.org/>`_, `Heurist <https://heuristnetwork.org/>`_, …), `JSON <https://www.json.org/json-fr.html>`_, ou `YAML <https://yaml.org/>`_.

:ref:`En savoir plus <heimdall:home>`



.. _projects_helios:

Helios
------

.. image:: https://sharedocs.huma-num.fr/wl/?id=FhFlfKYC3XbPrFNEwTiRcyaIq7vaGa6Q&fmode=download
   :target: https://gitlab.huma-num.fr/datasphere/helios/
   :height: 7em

Helios est un serveur web minimal qui vous permet de donner temporairement accès à vos fichiers locaux à un autre logiciel.
En particulier, à un logiciel tournant dans votre navigateur.

De par sa sécurité très minimale, Helios n'est à utiliser que pour communiquer avec des logiciels de confiance, et dans un environnement réseau maîtrisé (ou absent).

`En savoir plus <https://gitlab.huma-num.fr/datasphere/helios/>`_



projets « amis »
----------------

Les projets suivants utilisent tout ou partie des projets mentionnés ci-dessus.
Certains ont même été moteurs dans la conception et le développement des logiciels correspondants.

Ce sont ces partenariats scientifiques qui sont la raison d'être, d'une part de :ref:`tous ces logiciels <projects>`, mais aussi des :ref:`principes de gestion de projet <résilience>` qui les sous-tendent.

.. toctree::

   friends
