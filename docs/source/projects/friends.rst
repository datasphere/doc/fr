.. _friends:

Ces partenariats scientifiques sont la raison d'être, d'une part de :ref:`tous ces logiciels <projects>`, mais aussi des :ref:`principes de gestion de projet <résilience>` qui les sous-tendent.

Ils sont présentés dans l'ordre chronologiques de leurs collaborations.



.. _assassif:

El-Assassif
^^^^^^^^^^^

Ce programme de fouilles archéologiques se concentre sur la nécropole de la vallée d'El-Assassif.
Il fait notamment bon usage de plusieurs base de données pour ses travaux post-fouilles.

| Il est indispensable pour les chercheurs de cette équipe d'étudier à la fois les notices en bases de données mais aussi l'important corpus (entres autres photographique), avant, pendant et après chaque campagne de fouilles.
| Au cours de chaque chantier, et durant la période antérieure au dépôt de ce corpus sur un entrepôt institutionnel pérenne tel que Nakala [#nkl_assassif]_, il est nécessaire de pouvoir afficher dans des programmes tiers les fichiers de ce corpus, alors même que ceux-ci sont disponibles uniquement « en local ».

C'est ce besoin qui a initialement entraîné la création du logiciel :ref:`Helios <projects_helios>` lors de l'été 2022, créé en collaboration étroite avec les égyptologues `Cassandre HARTENSTEIN <https://egypte.unistra.fr/les-membres-de-lequipe/doctorants/cassandre-hartenstein/>`_ et `Frédéric COLIN <https://egypte.unistra.fr/les-membres-de-lequipe/enseignants/frederic-colin-professeur/>`_.

`En savoir plus sur El-Assassif <https://www.ifao.egnet.net/recherche/archeologie/assassif/>`_



.. _estrades:

Estrades
^^^^^^^^

.. image:: https://estrades.huma-num.fr/site/ui/estrades-color.svg
   :target: https://estrades.huma-num.fr/
   :width: 128px
   :align: center

| Estrades est une plateforme de soutien à la recherche proposant des solutions techniques, un accompagnement et des formations visant à soutenir le développement de projets en humanités numériques autour de l’édition de corpus.
| Entre autres, Estrades peut tirer le meilleur parti non seulement de corpus textuels au format  TEI [#tei]_, mais aussi de bases de données relationnelles renfermant davantage d'informations sur les entités nommées auxquelles les fichiers TEI font référence.

| C'est initialement pour pallier les limitations du logiciel de bases de données dont dépendait à l'époque le versant technique d'Estrades qu'a été créé le logiciel qui s'appelera, *in fine*, :ref:`Heimdall <projects_heimdall>`.
| Cependant, le périmètre d'Heimdall a été rapidement élargi grâce à la modélisation permise par :ref:`Hera <projects_hera>`.
 La capacité d'Estrades a accéder à de nouvelles sources de données a grandi d'autant.

Ce travail a été commencé fin 2023 [#heimdall_dates]_ dans le cadre de l'incubation du projet Estrades.
Le porteur d'Estrades, `Guillaume PORTE <https://orcid.org/0000-0001-8656-8227>`_ a notamment co-écrit l'expression du besoin et les spécifications initiales, et validé les premières versions d'xHeimdall.

`En savoir plus sur Estrades <https://estrades.huma-num.fr/>`_



.. _ckp:

Chi-Know-Po
^^^^^^^^^^^

.. image:: https://chiknowpo.hypotheses.org/files/2021/11/Logo.001.jpeg
   :target: https://chiknowpo.hypotheses.org/
   :height: 128px
   :align: center

*Chinese Knowledge in Poetry* (*alias* Chi-Know-Po, *alias* CKP) est un projet sinologique sur l'articulation entre les domaines de la poésie et du savoir dans la Chine médiévale.

| C'est le besoin pour :ref:`Estrades <estrades>` de pouvoir importer la base de données très complète de Chi-Know-Po qui a réellement lancé le développement de la première implémentation d'Heimdall [#heimdall_dates]_, programmée en langage XQuery [#basex]_.
| Par la suite, dans le cadre de la migration de la base de données CKP depuis un serveur privé vers un serveur institutionnel, sont apparues certaines questions de sécurité liées au mode d'accès à ladite base via des notebooks Jupyter [#jupyter]_, développés en Python [#python]_.

Lors des séances de travail communes avec `Marie BIZAIS <https://orcid.org/0000-0002-2426-2641>`_, porteuse du projet Chi-Know-Po, s'est imposée vers l'été 2024 l'idée de pyHeimdall, le portage d'Heimdall vers le langage Python.
L'objectif était de permettre à la base CKP de continuer à exister en toute sécurité, tout en permettant à Heimdall d'être plus facilement accessible à la communauté Python.
Et parce que Python, c'est bien [#xkcd_python]_.

`En savoir plus sur Chi-Know-Po <https://chiknowpo.hypotheses.org/>`_



.. rubric:: Références

.. [#nkl_assassif] Voici `quelques exemples de dépôts sur Nakala <https://nakala.fr/search/?q=asasif>`_ par les membres du projet Assassif.
.. [#tei] `Site officiel <https://tei-c.org/>`_ de la Text Encoding Initiative (TEI).
.. [#heimdall_dates] Le calendrier du projet Heimdall est lisible dans `son dépôt logiciel sur l'entrepôt Zenodo <https://doi.org/10.5281/zenodo.10665319>`_.
.. [#basex] En XQuery certes, mais plus précisément pour l'`environnement BaseX <https://basex.org/>`_.
.. [#jupyter]  `Site officiel du projet Jupyter <https://jupyter.org/>`_.
.. [#python]  `Site officiel du langage de programmation Python <https://www.python.org/>`_.
.. [#xkcd_python] Relevant XKCD: https://xkcd.com/353/
