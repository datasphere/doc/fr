:orphan:

==================
Entre mes oreilles
==================

Imaginez que chaque personne indispensable à la poursuite de vos travaux ...

* ... quitte son travail temporairement (congé, arrêt maladie, ...) ?
* ... choisisse de quitter votre projet définitivement (désaccord, retraite, démission, ...) ...
* ... en effaçant tous ses disques durs avant de partir ?
* ... fasse un burnout, une dépression, ou quel que soit le nom de cet enfer ?
* ... meure sans signe avant-coureur ?
* ... fasse une overdose ?
* ... soit retrouvée pendue dans son salon ?
* ... perde la tête et se jette du haut d'un pont ?

| Parfois, on préfère ne pas penser à ce qui pourrait arriver.
  Trouver plein de prétextes pour repousser cette réflexion :
  « ça n'arrive qu'aux autres », « c'est trop morbide », « on a largement le temps d'y penser, n'est-ce pas ? » ...
| Pour d'autres, cette réflexion est un mal nécessaire, car il en va de la survie de leurs travaux.
| Et pour au moins l'un d'entre nous, tout cela, c'est du vécu.

Croyez-vous que c'est lorsque ces moments terribles surviennent que les parties prenantes de votre projet auront l'envie, la compétence, le soutien et les informations nécessaires pour continuer avec un minimum de douleur supplémentaire ?
Se mettre à leur place, et tenter de leur faciliter la tâche, ne serait-ce qu'un peu, est-il vraiment « une perte de temps » ?

Nous ne sommes qu'un moment.
