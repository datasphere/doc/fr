.. index::
   single: dépôts
   double: logiciel; dépôt
.. _dépôts:

dépôts
======

Pensé pour être :ref:`modulaire <modularité>`, |project| est composé de plusieurs projets.
Chacun de ces projets peut être à son tour décomposé en modules (aussi appelés composants, ou sous-projets).

Dans une optique de :ref:`résilience` tout ces logiciels sont partagés publiquement,
de manière compatible avec les principes du logiciel libre et de la science ouverte.


.. contents::
   :local:


fichiers
--------

.. note::

   🚧 Cette section est en cours d'écriture ... 🚧


