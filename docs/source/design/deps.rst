.. index::
   single: dépendances
   single: librairies
   double: logiciel; dépendances
.. role:: strike
.. _dépendances:

dépendances
===========

Un logiciel comportant trop de dépendances est un frein à la :ref:`résilience`.

En effet, chacune des dépendances d'un logiciel :

#. suit son propre cycle de vie, son propre schéma de version ;
#. est dépendante de la machine ou le logiciel est installé ;
#. complique la reconstruction du logiciel à l'identique ;
#. peut « casser » le logiciel à tout moment ;
#. alourdit la taille du livrable logiciel ;
#. peut être source d'anomalies.

Toutes les dépendances n'entraînent pas tous ces inconvénients.
D'un autre coté, cette liste n'est pas exhaustive.
Et chaque dépendance a souvent *elle-même* des dépendances, multipliant les inconvénients potentiels de manière exponentielle.

Faire dépendre un logiciel d'une nouvelle dépendance peut offrir un gain de temps à court terme.
Cependant, ce gain de temps se paye toujours sur le long terme.
Plus la durée de vie d'un logiciel est longue, **moins** le rapport coût/bénéfice tend à être intéressant [DetteTechnique]_.

Or, |project| est pensé uniquement sur le long terme.
En conséquence, tous les composants de |project| portent une attention extrême à la gestion de leurs dépendances.


.. contents::
   :local:



dépendances directes ou indirectes
----------------------------------

Tous les désavantages listés précédemment s'appliquent aux dépendances d'un projet, mais aussi aux dépendances ... de ses dépendances.
Et cela peut aller très vite [#that_package]_ [#pount_server]_.

Si un projet ne dépend même que d'une seule dépendance, mais qui elle-même présente
un graphe de dépendances complexes, non seulement votre projet aura une chance
exponentielle de souffrir des désavantages listés, mais en plus vous serez peut-être
incapable de les contourner, puisque vous n'avez pas de contrôle sur le cycle de vie de
votre dépendance !



dépendances internes ou externes
--------------------------------

Ce chapitre évoque les désavantages pour un projet d'avoir trop de
dépendances *externes*, c'est à dire non gérées directement par les membres
dudit projet.

À l'inverse, un projet peut être développé de manière modulaire.
Dans ce cas, certains de ses composants dépendent d'autres composants, mais *internes* au projet.
Cela offre de nombreux avantages, décrits dans le chapitre :ref:`modularité`.



dépendances « de dev »
----------------------

Certains fichiers de déclarations de dépendances [#deps_file]_ permettent des différencier les dépendances principales (*dependencies*) et celles « de dev » (*devDependencies*).
La différence est que les dépendances « de dev » ne sont pas indispensables pour que le logiciel, une fois construit, remplisse son périmètre fonctionnel.

En particulier, constituent des dépendances « de dev » :

* les outils servant à construire le logiciel lui-même (*ie.* outils de *build*) ;
* les outils servant à vérifier la qualité du logiciel (*ie.* moteurs de tests, *linters*, ...) ;
* les outils servant à générer la documentation du logiciel ;
* les outils facilitant la vie des développeurs (*ie.* générateurs, fixtures, configurations, ...).

Les mêmes précautions s'appliquent aux dépendances « de dev » qu'aux autres.
Cela signifie qu'il ne faudrait pas déclarer n'importe quelle nouvelle dépendance, *même* sous prétexte que c'est « du dev ».
Chaque personne devrait pouvoir contribuer avec les outils qui ont sa faveur, sans être trop contrainte par les pratiques des autres.

Le partage de techniques est profitable et digne d'encouragements.
Cependant, il faut toujours que tout le monde puisse différencier ce qui est optionnel de ce qui ne l'est pas.

Accessoirement, pouvoir « faire sans » en cas de besoin est aussi un gage de :ref:`résilience`.



*Not Invented Here*
-------------------

Cet objectif de minimiser le nombre et l'importance des dépendances externes
ne doit pas être interprétée comme un témoignage du syndrome NIH [NIH]_.

La volonté n'est pas de tout redévelopper soi-même, ou de négliger les bénéfices de la mutualisation des développements.
L'enjeu est d'avoir une vraie réflexion sur la gestion de la dette technique [DetteTechnique]_, *avant* de la contracter.

En pratique, cette réflexion est en partie résumée par une série de questions :

#. Ai-je réellement besoin de cette fonctionnalité ?
#. Ai-je réellement besoin de l'implémenter de cette manière ?
#. Ai-je réellement besoin de cette dépendance,
   ou simplement d'un sous-ensemble des fonctionnalités qu'elle propose ?
#. Existe-t-il une librairie ne proposant que ces fonctionnalités dont j'ai besoin ?

Et ainsi de suite ...



conséquences pratiques
----------------------

Tous les composants de |project| portent une attention extrême à la gestion de leurs dépendances.

Dans le meilleur des cas, ils n'ont *aucune* dépendance.

Sinon, l'usage de *chacune* de leurs dépendances :

* doit être justifié : c'est à dire que la dépendance doit avoir un *bénéfice réel*,
  et que ce bénéfice doit être d'autant plus grand si la dépendance est lourde
  ou présente un risque relatif à sa pérénnité sur le long terme ;
* doit être déclaré explicitement et conformément aux standards de la technologie utilisée [#deps_file]_ ;
* doit être motivé au même endroit que sa déclaration [#deps_comments]_ ; c'est à dire :

  #. quel bénéfice *réel* offre la dépendance *pour le projet* ?
  #. où se trouve la documentation de la dépendance ?
  #. quelles sont les alternatives, et pourquoi ne sont-elles pas préférables ?
* doit être le plus flexible possible au niveau des versions acceptées
  –au mieux, *toutes* les versions de la dépendance
  (et au pire, au moins la version la plus récente)
  doivent permettre de rendre le logiciel fonctionnel.
  En particulier, la dépendance ne doit pas être « gelée » dans une version
  spécifique, même récente.


Dépendances et *versioning*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le fait que les composants du projet |project| ne souhaitent pas « geler » leurs (rares) dépendances
dans une version spécifique, même récente, peut sembler être contraire à
un environnement logiciel stable et reproductible.

Les outils de « gel » des versions ont deux objectifs :

#. stabiliser le développement et la maintenance du logiciel, afin que
   chaque membre de l'équipe de développement, et chaque cible de production,
   ait le même environnement en termes de dépendances ;
#. rendre le logiciel et son environnement reproductibles.

Ces deux objectifs partent du principe que le cycle de vie du logiciel est
découpé en deux phases : une phase de développement active, et une phase
d'inactivité, souvent bien plus longue que la phase de développement.

Cette hypothèse est nulle et non avenue si le logiciel est continuellement en
développement, sur un temps *très* long, c'est à dire suffisamment long pour
que les versions de dépendances qui étaient d'actualité quand vous avez
commencé vos développements soient obsolètes aujourd'hui.

Mais alors, pourquoi |project| choisit de ne pas faire « comme tout le monde »,
c'est à dire faire évoluer ponctuellement des dépendances « gelées », de
manière choisie et correspondant au calendrier du projet ?

La réponse est qu'avec une telle manière de faire, toutes les dépendances ont tendance à « casser »
en même temps, et qu'une telle situation peut vite devenir infernale à corriger.
Or, les tâches chronophages et risquées représentent un investissement en
ressources humaines dont |project| est dénué.

Donc, :strike:`si` quand un composant doit casser, mieux vaut qu'il casse de manière spectaculaire,
localisée, rapidement détectable, et que le fonctionnement normal soit facilement rétabli.

En conséquence, vous ne devriez jamais voir de ``package-lock.json``, de ``pnpm-lock.json``,
de ``composer.lock`` et consorts à la racine des composants de |project|.
Si vous :ref:`contribuez <contribuer>` à |project| (merci! ᵔᴥᵔ), mieux vaut vous assurer
que ceux générés localement ne vous empêchent pas d'upgrader souvent vos packages.

* Notre environnement reproductible minimal, c'est *tout dans sa dernière version* (stable).
* Notre environnement reproductible maximal, c'est *tout dans toutes ses versions* (stables).

Si une dépendance est incapable de remplir le fonctionnel attendu d'elle dans sa dernière version stable,
c'est probablement qu'il est temps de songer à s'en défaire au profit d'une alternative plus stable ...




.. rubric:: Références

.. [#that_package]
   Exemple pris (presque) au hasard, ce paquet NPM :
   `react-angular-component <https://www.npmjs.com/package/react-angular-component>`_.
   Il présente 521 dépendances directes.
   Allons plus loin, et analysons son graphe de dépendances complet :
   https://npmgraph.js.org/?q=react-angular-component
   En comptant les dépendances indirectes, cela monte à 721,
   dont 89 sont présents dans de multiples versions (potentiellement incompatibles),
   et (à l'heure où j'écris, fin février 2024) 26 sont sont obsolètes.
   Cela représente 417 mainteneurs différents.
   Qui peut gérer une telle complexité ?
   Comment prétendre qu'elle est exempte d'anomalies ?
   Toujours à l'heure où j'écris, le logiciel ne semble plus maintenu.
   *Tu m'étonnes ...*
.. [#pount_server]
   Autre exemple, en Python cette fois.
   Voici l'état des dépendances d'un projet `en juillet 2020 <https://git.unistra.fr/community/pount/pount-api/-/tree/83afc5d717544a3565e7180cc003944c95215c8f/requirements>`_.
   Cela fait 14 dépendances, pour la plupart expliquées et logiquement organisées.
   Le même projet `quelques années plus tard <https://git.unistra.fr/community/pount/pount-api/-/tree/a2a9e0feb7a299c61f63e437e874e35fbbef7dd2/requirements>`_ ...
   Plus de 100 dépendances, répétées, mélangées, non commentées, qui ne supportent que les anciennes versions de Python.
   Quel élargissement du périmètre fonctionnel peut justifier ceci ?
.. [#deps_file]
   Des fichiers de déclaration de dépendances reconnus sont, par exemple :
   ``requirements.txt`` (et assimilés) pour Python,
   ``package.json`` pour Node,
   ``composer.json`` pour PHP,
   et ainsi de suite.
   Ces fichiers sont utilisés par de nombreux gestionnaires de paquets pour une
   gestion plus efficaces des dépendances d'un projet.
.. [#deps_comments]
   Voici `un exemple`_ de manière de commenter un fichier ``package.json``.
   Toutes les dépendances sont listées par type, dans un ordre logique.
   Les commentaires se trouvent au même endroit que la déclaration.
.. _`un exemple`: https://gitlab.huma-num.fr/datasphere/hera/client.js/-/blob/main/package.json
