.. index::
   single: résilience
   single: pérénité
   double: logiciel; résilience
.. _résilience:

résilience
==========

.. note::

   🚧 Cette section est en cours d'écriture ... 🚧

* une couverture de tests (unitaires *et* fonctionnels) stricte
  et aussi exhaustive que possible ;
* pas ou peu de :ref:`dépendances` ;
