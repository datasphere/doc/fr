.. index::
   single: modularité
   double: logiciel; modules
   double: logiciel; composants
.. _modularité:

modularité
==========

|project| est un système modulaire.
Cela signifie que ses différents composants, ou modules, peuvent fonctionner indépendamment l'un de l'autre.
Chacun peut (et *doit*) communiquer avec n'importe quel autre logiciels à travers des interfaces correctement spécifiées.


.. contents::
   :local:



bénéfices
---------

La décomposition d'un système en modules plus simples peut en réduire la complexité tout en en améliorant la maintenabilité, la flexibilité, la réusabilité et l'évolution.
Par exemple, un environnement modulaire correctement conçu permet entre autres de :

* remplacer, rajouter ou retirer certains modules sans impacter le reste du système ;
* gérer une charge de travail variant (souvent à la hausse) au cours du temps ;
* faciliter l'analyse des anomalies, leur correction, et la testabilité générale du système ;
* créer de nouveaux systèmes, dérivés de l'existant, de manière plus efficiente.

Puisqu'elle permet l'adaptation à des contextes (technologiques, fonctionnels et humains) variés, la modularité d'un logiciel est une condition indispensable de sa :ref:`résilience`.

défis
-----

Cependant, la modularité ne s'atteint pas sans efforts.

En première approche, un système modulaire peut sembler plus complexe.
Cependant, si chaque module est correctement documenté, il est plus facile de comprendre le système progressivement, module par module.
Cela permet de comprendre le système à la fois dans son ensemble mais aussi dans le détail, et d'éviter l'effet « boîte noire ».

La coordination entre les modules peut aussi poser difficulté.
Il est indispensable de correctement gérer les évolutions d'interface, les :ref:`dépendances`, différentes configurations et versions de modules, d'assurer la (rétro-)compatibilité, ...

techniques
----------

Il n'y a pas de recette miracle, et encore moins de manière unique d'atteindre tous les bénéfices de la modularité, en évitant tous les risques.
De manière non-limitative, on peut néanmoins citer les bonnes pratiques suivantes :

#. décomposer de manière appropriée aux fonctionnalités souhaitées du système ;
#. définir des interfaces claires et cohérentes, à l'épreuve du temps ;
#. éviter les couplages ; adopter un niveau approprié de redondance ;
#. éviter à tout prix, et le cas échéant, bien anticiper et documenter, les ruptures d'interfaces ;
#. documenter de manière exhaustive et à jour ;
#. tester de manière exhaustive, continue, incrémentale ;
#. prendre en compte le futur : en d'autre termes, s'assurer que tout ce qu'on a oublié soit pris en compte quand même.

En résumé, il suffit de faire preuve de discipline, aussi bien envers soi-même qu'envers les utilisateurs du système.
