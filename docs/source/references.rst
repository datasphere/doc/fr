.. index::
   single: références
   single: bibliographie
.. _références:

références
==========


.. [DetteTechnique]
   Le concept de *dette technique* a été originellement formulé par Ward Cunningham.
   Pour en savoir plus, lire par exemple cet article de Martin Fowler :
   https://martinfowler.com/bliki/TechnicalDebt.html

.. [NIH]
   Le « syndrome » NIH : https://fr.wikipedia.org/wiki/Not_invented_here
