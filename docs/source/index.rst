#########
|project|
#########

.. _home:

.. note::

   🚧 Cette documentation est en cours d'écriture ... 🚧

.. toctree::
   :hidden:

   /design/this
   /contributing
   /design/resilience
   /projects/list
   /faq
   /references
   /genindex
